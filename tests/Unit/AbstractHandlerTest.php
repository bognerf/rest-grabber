<?php


namespace Bognerf\RestGrabber\Tests;


use Bognerf\RestGrabber\Exceptions\HandlerException;
use Bognerf\RestGrabber\Handlers\PlainJson;
use PHPUnit\Framework\TestCase;

class AbstractHandlerTest extends TestCase
{

    /**
     * @var string
     */
    protected $defaultBody;
    /**
     * @var string
     */
    protected $nonJsonBody;

    public function setUp(): void
    {
        parent::setUp();
        $this->defaultBody = '{
            "something":"what", 
            "differentthing": "whatelse", 
            "deeper": {"another-value":"in-more-dimensions"}
            }';
        $this->nonJsonBody = 'didel,dadel,dudel';
    }

    public function testParsingValidJson()
    {
        $handler = new PlainJson();
        $handler->parse($this->defaultBody);
        $this->assertEquals(json_decode($this->defaultBody, true), $handler->get());
    }

    public function testParsingInvalidJson()
    {
        $handler = new PlainJson();
        $this->expectException(HandlerException::class);
        $handler->parse($this->nonJsonBody);
    }

}