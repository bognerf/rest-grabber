<?php


namespace Bognerf\RestGrabber\Tests;

use BlastCloud\Guzzler\UsesGuzzler;
use Bognerf\RestGrabber\Exceptions\ContentTypeException;
use Bognerf\RestGrabber\Exceptions\HandlerException;
use Bognerf\RestGrabber\Exceptions\ResponseException;
use Bognerf\RestGrabber\Exceptions\RestGrabberException;
use Bognerf\RestGrabber\Exceptions\ValueObjectException;
use Bognerf\RestGrabber\Grabber;
use Bognerf\RestGrabber\Url;
use Cache\Adapter\PHPArray\ArrayCachePool;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Validator\Constraints\Count;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Validator\Constraints\Type;

/**
 * Class GrabberTest
 *
 * @package Bognerf\RestGrabber\Tests
 */
class GrabberTest extends TestCase
{
    use UsesGuzzler;

    /**
     * @var Grabber
     */
    public $grabber;

    protected $fakeClient;
    protected $url;
    protected $defaultBody = '';
    protected $cdnjsBody = '';
    protected $kostenstellenBody = '';
    protected $combinedBody;

    protected $cacheDummy = null;

    const TOKEN = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c';

    public function setUp(): void
    {
        parent::setUp();
        $this->fakeClient = $this->guzzler->getClient([]);
        $this->url = new Url('https://test.florianbogner.de/api/');
        $this->grabber = new Grabber($this->url, $this->fakeClient);
        $this->defaultBody = file_get_contents(__DIR__ . '/../data/default.json');
        $this->cdnjsBody = file_get_contents(__DIR__ . '/../data/cdnjs.json');
        $this->kostenstellenBody = file_get_contents(__DIR__ . '/../data/kostenstellen.json');
        $this->combinedBody = file_get_contents(__DIR__ . '/../data/combined.json');

        $this->cacheDummy = new ArrayCachePool();
    }

    public function httpMethodsProvider()
    {
        return [
            ['GET', true],
            ['POST', true],
            ['DELETE', true],
            ['get', true],
            ['post', true],
            ['PUT', false],
            ['OPTIONS', false],
            ['NONSENSE', false],
        ];
    }

    /**
     * @dataProvider httpMethodsProvider
     */
    public function testAllowedHttpMethods(string $method, $raisesException)
    {
        if ($raisesException !== true) {
            $this->expectException(RestGrabberException::class);
        }
        $this->grabber->setMethod($method);
        $this->assertSame(strtoupper($method), $this->grabber->getMethod());
    }

    public function testIfResponseIsErroneous()
    {
        $this->guzzler->queueResponse(
            new ResponseException('URL does not exist'),
            new Response(500, [], 'ERROR')
        );
        while ($this->guzzler->queueCount() > 0) {
            $this->expectException(RestGrabberException::class);
            $this->grabber->get()->grab();
        }
    }

    public function grabberFunctionalityMethodsProvider()
    {
        return [
            ['GET', null],
            ['POST', ['field' => 'something', 'value' => 'what']],
            ['DELETE', ['field' => 'something', 'value' => 'what']],
        ];
    }

    /**
     * @throws RestGrabberException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Psr\SimpleCache\InvalidArgumentException
     * @dataProvider grabberFunctionalityMethodsProvider
     */
    public function testBaseGrabberFunctionality($method, $payloadCheck)
    {
        $this->guzzler->queueResponse(
            (new Response(200, [], $this->defaultBody))->withHeader('Content-Type', Grabber::CONTENT_TYPE),
            (new Response(200, [], $this->defaultBody))->withHeader(
                'Content-Type',
                Grabber::CONTENT_TYPE . '; charset=utf-8'
            ) // standard header with charset
        );

        while ($this->guzzler->queueCount() > 0) {
            $this->grabber->setMethod($method);
            $grabbed = $this->grabber->grab();

            self::assertEquals(200, $this->grabber->getResponse()->getStatusCode());

            if ($payloadCheck !== null && is_array($payloadCheck)) {
                self::assertEquals($payloadCheck['value'], $grabbed->data()[$payloadCheck['field']]);
            }
        }
    }

    public function testGrabberForcesContentType()
    {
        $this->guzzler->queueResponse(
            (new Response(200))->withHeader('Content-Type', 'text/plain')
        );

        $this->expectException(ContentTypeException::class);
        $this->grabber->get()->grab();
    }

    public function testJsonResponseSwitch()
    {
        $data = ['some' => 'data', 'different' => 'information'];
        $this->grabber->post($data);
        $this->assertTrue($this->grabber->usesJsonRequest());
        $this->assertJson($this->grabber->getPayload());
        $this->grabber->useJsonRequest(false);
        $this->assertFalse($this->grabber->usesJsonRequest());
        $this->assertSame(http_build_query($data), $this->grabber->getPayload());
    }

    public function testHandlerKeepsOriginalData()
    {
        $this->guzzler->queueResponse(
            (new Response(200, [], $this->defaultBody))->withHeader('Content-Type', Grabber::CONTENT_TYPE)
        );

        $this->grabber->get()->grab();
        $this->assertSame($this->defaultBody, $this->grabber->getHandler()->getRaw());
        $this->assertSame($this->defaultBody, $this->grabber->contents());
    }

    public function testHandlerExpectsJson()
    {
        $this->guzzler->queueResponse(
            (new Response(200, [], $this->defaultBody))->withHeader('Content-Type', Grabber::CONTENT_TYPE),
            (new Response(200, [], 'A non-JSON string'))->withHeader('Content-Type', Grabber::CONTENT_TYPE)
        );

        // With valid JSON (first Response)
        $this->grabber->get()->grab();
        $this->assertSame(json_decode($this->defaultBody, true), $this->grabber->data());

        // With invalid JSON (second Response)
        $this->expectException(HandlerException::class);
        $this->grabber = new Grabber($this->url, $this->fakeClient);
        $this->grabber->get()->grab();
    }

    public function testObligatoryFieldsAreRecognized()
    {
        $this->guzzler->queueResponse(
            (new Response(200, [], $this->defaultBody))->withHeader('Content-Type', Grabber::CONTENT_TYPE)
        );

        $this->grabber->getHandler()->setObligatoryFields(
            [
                'something' => [],
                'differentthing' => [],
                'deeper.another-value' => [],
            ]
        );
        $this->grabber->get()->grab();
        $this->assertTrue($this->grabber->getHandler()->allObligatoryFieldArePresent());
    }

    public function testMissingObligatoryFieldsThrowException()
    {
        $this->guzzler->queueResponse(
            (new Response(200, [], $this->defaultBody))->withHeader('Content-Type', Grabber::CONTENT_TYPE)
        );

        $this->grabber->getHandler()->setObligatoryFields(
            [
                'something' => [],
                'non-existent-key' => [],
                'deeper.underground' => [],
            ]
        );

        $this->expectException(HandlerException::class);

        $this->grabber->get()->grab();
        $this->assertFalse($this->grabber->getHandler()->allObligatoryFieldArePresent());
    }

    public function testValueObjectsBeingGenerated()
    {
        $this->guzzler->queueResponse(
            (new Response(200, [], $this->cdnjsBody))->withHeader('Content-Type', Grabber::CONTENT_TYPE),
            (new Response(200, [], $this->cdnjsBody))->withHeader('Content-Type', Grabber::CONTENT_TYPE)
        );

        $this->grabber->getHandler()->setObligatoryFields(
            [
                'assets' => [
                    new Count(['min' => 30])
                ],
                'assets.0.version' => [],
                'name' => [
                    new NotNull(),
                ],
                'author' => [
                    new NotNull(),
                ]
            ]
        );
        $this->grabber->getHandler()->addValueObjectsRoot(['assets' => []]);
        $this->grabber->get()->grab();

        // 38 because we know how many items there are, see ../data/cdnjs.json
        self::assertCount(38, $this->grabber->getHandler()->valueObjects('assets'));

        foreach ($this->grabber->valueObjects('assets') as $obj) {
            self::assertIsString($obj->getVersion());
        }

        ////// Now test to see if non-existent root element throws an exception
        $this->expectException(ValueObjectException::class);
        $this->grabber->getHandler()->addValueObjectsRoot(['rocket' => []]);
        $this->grabber->get()->grab();
    }

    public function testValueObjectsValidation()
    {
        $this->guzzler->queueResponse(
            (new Response(200, [], $this->cdnjsBody))->withHeader('Content-Type', Grabber::CONTENT_TYPE),
            (new Response(200, [], $this->cdnjsBody))->withHeader('Content-Type', Grabber::CONTENT_TYPE)
        );

        $this->grabber->getHandler()->setObligatoryFields(
            [
                'assets' => [
                    new Count(['min' => 1])
                ],
                'assets.0.version' => [],
                'name' => [
                    new NotNull(),
                ],
                'author' => [
                    new NotNull(),
                ]
            ]
        );
        $this->grabber->getHandler()->addValueObjectsRoot([
                'assets' =>
                    [
                        'version' => [
                            new Length(['max' => 24]),
                        ],
                        'files' => [
                            new Count(['min' => 4])
                        ],
                    ],
            ]
        );
        $this->grabber->get()->grab();
        // 38 because we know how many items there are, see ../data/cdnjs.json
        $this->assertCount(38, $this->grabber->valueObjects('assets'));
    }

    public function testUnnamedValueObjectsRoot()
    {
        $this->guzzler->queueResponse(
            (new Response(200, [], $this->kostenstellenBody))->withHeader('Content-Type', Grabber::CONTENT_TYPE)
        );

        $this->grabber->getHandler()->addValueObjectsRoot([
                '*' =>
                    [
                        'rzaccount' => [
                            new Regex(['pattern' => '/^([a-z]){3}([0-9]){5}$/'])
                        ],
                        'id' => [
                            new Type(['type' => 'numeric']),
                        ],
                    ],
            ]
        );
        $this->grabber->get()->grab();
        $this->assertTrue($this->grabber->getHandler()->hasValueObjectRoots());
        $this->assertTrue($this->grabber->getHandler()->hasValueObjectRoot('*'));
        $this->assertCount(75, $this->grabber->valueObjects('*'));
    }

    public function testMultipleValueObjectRoots()
    {
        $this->guzzler->queueResponse(
            (new Response(200, [], $this->combinedBody))->withHeader('Content-Type', Grabber::CONTENT_TYPE)
        );
        $this->grabber->getHandler()->addValueObjectsRoot(['assets' => []]);
        $this->grabber->getHandler()->addValueObjectsRoot(['groups' => []]);

        $this->grabber->get()->grab();

        $this->assertTrue($this->grabber->getHandler()->hasValueObjectRoots());
        $this->assertTrue($this->grabber->getHandler()->hasValueObjectRoot('assets'));
        $this->assertTrue($this->grabber->getHandler()->hasValueObjectRoot('groups'));
        $this->assertTrue($this->grabber->getHandler()->hasValueObjects());
        $this->assertEquals(3, $this->grabber->getHandler()->numValueObject('assets'));
        $this->assertEquals(2, $this->grabber->getHandler()->numValueObject('groups'));
        $this->assertEquals(5, $this->grabber->getHandler()->numValueObjects());
        $this->assertCount(2, $this->grabber->getHandler()->getValueObjectsRoots());

        $this->assertFalse($this->grabber->getHandler()->hasValueObjectRoot('didel'));
        $this->assertFalse($this->grabber->getHandler()->hasValueObjectRoot('dadel'));

    }

    public function testBearerTokenIsUsed()
    {
        $this->guzzler->queueResponse(
            (new Response(200, [], $this->defaultBody))->withHeader('Content-Type', Grabber::CONTENT_TYPE),
            (new Response(200, [], $this->defaultBody))->withHeader('Content-Type', Grabber::CONTENT_TYPE)
        );


        $this->assertFalse($this->grabber->hasBearerToken());
        $this->grabber->get()->grab();
        $this->assertFalse($this->grabber->getRequest()->hasHeader('Authorization'));

        $this->guzzler->expects($this->once())->withHeader('Authorization', 'Bearer ' . self::TOKEN);
        $this->grabber->setBearerToken(self::TOKEN);
        $this->assertTrue($this->grabber->hasBearerToken());

        $this->grabber->grab();
        $this->assertTrue($this->grabber->getRequest()->hasHeader('Authorization'));
    }

    public function testBodyTypes()
    {
        $this->guzzler->queueResponse(
            (new Response(200, [], $this->defaultBody))->withHeader('Content-Type', Grabber::CONTENT_TYPE),
            (new Response(200, [], $this->defaultBody))->withHeader('Content-Type', Grabber::CONTENT_TYPE)
        );

        $body = [
            'username' => 'USER',
            'password' => 'PASSWORD',
        ];

        $this->guzzler->expects($this->once())->withBody(json_encode($body));
        $this->grabber->post($body)->grab();

        $this->guzzler->expects($this->once())->withBody(http_build_query($body));
        $this->grabber->useJsonRequest(false);
        $this->grabber->post($body)->grab();

    }


    public function testCaching()
    {
        $this->guzzler->queueResponse(
            (new Response(200, [], $this->defaultBody))->withHeader('Content-Type', Grabber::CONTENT_TYPE),
            (new Response(200, [], $this->defaultBody))->withHeader('Content-Type', Grabber::CONTENT_TYPE)
        );

        // Assure cache is empty by default and won't be used
        self::assertFalse($this->grabber->canCache());
        self::assertFalse($this->grabber->hasCache());
        self::assertFalse($this->grabber->isCached());
        // Set the cache
        $this->grabber->setCache($this->cacheDummy);
        // Set method to GET as other verbs don't support caching
        $this->grabber->setMethod('GET');
        // Assure cache is now active
        self::assertTrue($this->grabber->hasCache());

        $this->grabber->get()->grab();
        // Assure cache is filled after request
        $this->assertTrue($this->grabber->isCached());
        // Force disabling of cache
        $this->grabber->skipCache();
        $this->grabber->grab();
        // Assure cache is empty if skipCache() was called
        $this->assertFalse($this->grabber->isCached());
    }

    public function testCachingTtl()
    {
        $ttl = 1000;
        $this->assertEquals(Grabber::DEFAULT_TTL, $this->grabber->getTtl());
        $this->grabber->setTtl($ttl);
        $this->assertEquals($ttl, $this->grabber->getTtl());
    }

    public function testCA()
    {
        $this->assertNull($this->grabber->getCa());
        $this->assertTrue($this->grabber->verify());
        $this->grabber->setCa(__DIR__ . '/../data/dummyCA.pem');

        $this->assertStringContainsString('dummyCA.pem', $this->grabber->getCa());
        $this->assertNotTrue($this->grabber->verify());
        $this->assertFileExists($this->grabber->verify());
    }
}
