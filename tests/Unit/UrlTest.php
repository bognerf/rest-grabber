<?php


namespace Bognerf\RestGrabber\Tests;

use Bognerf\RestGrabber\Exceptions\UrlException;
use Bognerf\RestGrabber\Url;
use PHPUnit\Framework\TestCase;

class UrlTest extends TestCase
{

    public function urlProvider()
    {
        return [
            ['https://api.cdnjs.com/libraries/axios', true],
            ['http://api.cdnjs.com/libraries/axios', false],
            ['NOt an UrL at ALl', false],
            ['fb@florianbogner.de', false],
        ];
    }

    /**
     * @param        string $url
     * @param        bool   $isUrl
     * @dataProvider urlProvider
     * @throws       UrlException
     */
    public function testUrlValidity(string $url, bool $isUrl)
    {
        if (!$isUrl) {
            $this->expectException(UrlException::class);
        }

        new Url($url);
        $this->assertNotFalse(filter_var($url, FILTER_VALIDATE_URL));
    }
}
