<?php


namespace Bognerf\RestGrabber\Tests;


use Bognerf\RestGrabber\Exceptions\ValueObjectException;
use Bognerf\RestGrabber\ValueObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Type;
use Symfony\Component\Validator\Constraints\Url;

class ValueObjectTest extends TestCase
{

    protected $rules;
    protected $data;
    protected $unsuccessfulRules;

    public function setUp(): void
    {
        parent::setUp();

        $this->data = [
            'name' => 'Florian Bogner',
            'age' => 39,
            'email' => 'fb@florianbogner.de',
        ];

        $this->rules = [
            'name' => [
                new NotBlank()
            ],
            'age' => [
                new Type(['type' => 'numeric'])
            ],
            'email' => [
                new Email()
            ],
        ];

        $this->unsuccessfulRules = [
            'name' => [
                new Type(['type' => 'numeric']),
            ],
            'email' => [
                new Url(),
            ]
        ];

    }

    public function testBasicBehaviour()
    {
        $vo = new ValueObject($this->rules);
        $vo->setData($this->data);

        // testing data is OK
        $this->assertEquals($this->data, $vo->toArray());
    }

    /**
     * @throws \Bognerf\RestGrabber\Exceptions\ValueObjectException
     */
    public function testMagicMethods()
    {
        $vo = new ValueObject($this->rules);
        $vo->setData($this->data);
        // testing VO has element and magic getter
        foreach (array_keys($this->data) as $key) {
            $this->assertTrue($vo->has($key));
            $this->assertSame($vo->$key, $this->data[$key]);
            $accessor = 'get' . ucfirst($key);
            $this->assertSame($vo->__call($accessor, ''), $this->data[$key]);
            $this->assertSame($vo->$accessor(), $this->data[$key]);
        }
    }

    public function testInvalidRulesAreRecognized()
    {
        $this->rules['dummy'] = null;
        $vo = new ValueObject($this->rules);
        $this->expectException(ValueObjectException::class);
        $vo->setData($this->data);
    }

    public function testConstraintsAreForced()
    {
        $vo = new ValueObject($this->unsuccessfulRules);
        $this->expectException(ValueObjectException::class);
        $vo->setData($this->data);
    }

    public function testInvalidMagicMethods()
    {
        $vo = new ValueObject($this->rules);
        $vo->setData($this->data);

        $this->assertNull($vo->getSomething());
        $this->assertNull($vo->postName());
    }

}