<?php

namespace Bognerf\RestGrabber;

use Bognerf\RestGrabber\Exceptions\ContentTypeException;
use Bognerf\RestGrabber\Exceptions\ResponseException;
use Bognerf\RestGrabber\Exceptions\RestGrabberException;
use Bognerf\RestGrabber\Handlers\PlainJson;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Psr\Http\Message\ResponseInterface;
use Psr\SimpleCache\CacheInterface;

class Grabber
{
    /**
     * @var Url
     */
    protected $url;

    /**
     * @var Client
     */
    protected $client;

    /** @var CacheInterface */
    protected $cache;
    /** @var bool */
    protected $skipCache = false;
    /** @var int Cache TTL in seconds */
    protected $ttl = -1;

    /**
     * @var string
     */
    protected $method;

    protected $headers = [];

    protected $payload = [];

    protected $bearerToken = '';

    /** @var Request */
    protected $request;
    protected $response;

    protected $jsonRequest = true;

    protected $contents = '';

    protected $ca = null;

    protected $error = false;
    protected $errorMessage = '';

    /**
     * @var Handler
     */
    protected $handler;

    protected $parsedData = [];


    public const TIMEOUT = 3;
    public const CONTENT_TYPE = 'application/json';
    public const ACCEPT_TYPE = 'application/json';
    public const CACHE_SEPARATOR = '|';
    public const DEFAULT_TTL = 300;

    protected const METHODS = 'GET|POST|DELETE';

    public function __construct(Url $url, Client $client = null)
    {
        $this->setUrl($url);
        $this->client = $client ?? new Client();
        $this->_initHeaders();
        $this->setHandler(new PlainJson());
    }


    /**
     * Get the current guzzlehttp\Client
     *
     * @return Client
     */
    public function client(): Client
    {
        return $this->client;
    }

    /**
     * Perform a request via POST and add the post body as array $data
     * which will be json_encode'd
     *
     * @param array $data
     * @return Grabber
     * @throws RestGrabberException
     */
    public function post($data): Grabber
    {
        $this->payload = $data;

        $this->setMethod('post');
        return $this;
    }

    /**
     * Peform a request via GET
     *
     * @return Grabber
     * @throws RestGrabberException
     */
    public function get(): Grabber
    {
        $this->setMethod('get');
        return $this;
    }

    /**
     * Peform a request via DELETE
     *
     * @return Grabber
     * @throws RestGrabberException
     */
    public function delete($data): Grabber
    {
        $this->payload = $data;
        $this->setMethod('get');
        return $this;
    }


    public function _initHeaders()
    {
        $this->headers['Accept'] = self::ACCEPT_TYPE;
    }

    /**
     * Send the request
     * The response content is then available via Grabber::content(),
     * the parsed data can be retrieved by Grabber::data(),
     * and potential ValueObjects[] can be retrieved by Grabber::valueObjects()
     *
     * @return Grabber
     * @throws RestGrabberException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function grab(): Grabber
    {
        if ($this->isCached()) {
            $this->contents = $this->getCache()->get($this->cacheId());
            $this->handle();
            return $this;
        }

        try {
            $this->request = new Request(strtoupper($this->method), (string)$this->getUrl(), $this->headers);

            if ($this->hasBearerToken()) {
                $this->request = $this->request->withHeader('Authorization', 'Bearer ' . $this->getBearerToken());
            }

            $this->response = $this->client()->send(
                $this->request,
                [
                    'timeout' => self::TIMEOUT,
                    'body' => $this->getPayload(),
                    'verify' => $this->verify(),
                ]
            );
        } catch (ClientException | ServerException $e) {
            $this->error = true;
            $this->errorMessage = $e->getMessage();
            $this->response = $e->getResponse();
            $this->contents = (string)$e->getResponse()->getBody();
            return $this;
        } catch (\Exception $e) {
            throw new RestGrabberException('Got `' . $e->getMessage() .
                '` while retrieving (' . $this->method . ') ' . $this->getUrl());
        }

        $this->responseValidation();

        $this->contents = (string)$this->response->getBody();

        if ($this->hasCache()) {
            $this->getCache()->set($this->cacheId(), $this->contents, $this->getTtl());
        }
        $this->handle();


        return $this;
    }

    /**
     * Validates that the response is valid and has the correct Content-Type
     *
     * @throws RestGrabberException
     */
    protected function responseValidation(): void
    {
        if (!is_a($this->response, Response::class)) {
            throw new ResponseException('No response or erroneous response');
        }

        if (!$this->hasAppropriateContentType()) {
            throw new ContentTypeException('Content-Type is not ' . self::CONTENT_TYPE);
        }
    }

    /**
     * Searches through all headers `Content-Type` to find a valid value defined
     * in self::CONTENT_TYPE
     *
     * @return bool
     */
    protected function hasAppropriateContentType(): bool
    {
        foreach ($this->getResponse()->getHeader('Content-Type') as $header) {
            if (0 === strpos($header, self::CONTENT_TYPE)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Define whether the request body (POST) should be JSON encoded $json=true
     * or a common query string $json=false
     * Defaults to true
     *
     * @param bool $json
     */
    public function useJsonRequest(bool $json)
    {
        $this->jsonRequest = $json;
    }

    /**
     * Returns true, if the current instance's post body will be JSON-encoded
     * @return bool
     */
    public function usesJsonRequest(): bool
    {
        return $this->jsonRequest;
    }

    /**
     * Gets the currently set request payload as JSON or query string,
     * depending on the setting of self::$jsonRequest
     *
     * @return string
     */
    public function getPayload(): string
    {
        if ($this->jsonRequest !== true) {
            return http_build_query($this->payload);
        }

        return json_encode($this->payload);
    }

    /**
     * Set the payload for the current request (POST)
     *
     * @param array $payload
     */
    public function setPayload(array $payload): void
    {
        $this->payload = $payload;
    }

    /**
     * The Reponse object after a successful request
     *
     * @return Response
     */
    public function getResponse(): Response
    {
        return $this->response;
    }

    public function getRequest(): Request
    {
        return $this->request;
    }

    /**
     * Get the Url object
     *
     * @return Url
     */
    public function getUrl(): Url
    {
        return $this->url;
    }

    /**
     * Set the Url object, which constitutes the requests target address
     *
     * @param Url $url
     */
    public function setUrl(Url $url): void
    {
        $this->url = $url;
    }

    /**
     * @return CacheInterface
     */
    protected function getCache(): CacheInterface
    {
        return $this->cache;
    }

    /**
     * @param CacheInterface $cache
     */
    public function setCache(CacheInterface $cache): void
    {
        $this->cache = $cache;
    }

    /**
     * Returns true if method is GET, because other methods should not cache...
     * @return bool
     */
    public function canCache(): bool
    {
        if ($this->skipCache === true) {
            return false;
        }
        try {
            return strtoupper($this->getMethod()) === 'GET';
        } catch (\Throwable $e) {
            return false;
        }

    }

    public function hasCache(): bool
    {
        if (!$this->canCache()) {
            return false;
        }
        return is_a($this->cache, CacheInterface::class);
    }

    public function cacheId(): string
    {
        return md5($this->getUrl()->getUrl() . self::CACHE_SEPARATOR . $this->getPayload());
    }

    public function isCached(): bool
    {
        return !$this->skipCache &&
            $this->hasCache() &&
            $this->getCache()->get($this->cacheId()) !== null;
    }

    public function skipCache()
    {
        $this->skipCache = true;
    }

    /**
     * @return int
     */
    public function getTtl(): int
    {
        if ($this->ttl < 0) {
            return self::DEFAULT_TTL;
        }

        return $this->ttl;
    }

    /**
     * @param int $ttl
     */
    public function setTtl(int $ttl): void
    {
        $this->ttl = $ttl;
    }


    /**
     * Get the raw string content of the response body
     *
     * @return string
     */
    public function contents(): string
    {
        return $this->contents;
    }

    /**
     * Get the method to use for the request (GET|POST)
     *
     * @return string
     */
    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * Set the method to use for a request. Value is evaluated against self::METHODS
     *
     * @param string $method
     * @throws RestGrabberException
     */
    public function setMethod(string $method): void
    {
        $method = strtoupper($method);
        if (preg_match('/^(' . self::METHODS . ')$/', $method) !== 1) {
            throw new RestGrabberException('Method ' . $method . ' is not allowed');
        }
        $this->method = $method;
    }

    /**
     * Returns the data handler associated with this Grabber
     *
     * @return Handler
     */
    public function getHandler(): Handler
    {
        return $this->handler;
    }

    /**
     * Set a data handler
     *
     * @param Handler $handler
     */
    public function setHandler(Handler $handler): void
    {
        $this->handler = $handler;
    }

    /**
     * Pushes the response body to the handler to be parsed
     * and for ValueObjects to be created if possible
     *
     * @throws Exceptions\HandlerException
     * @throws Exceptions\ValueObjectException
     */
    protected function handle(): void
    {
        $this->parsedData = $this->handler->parse($this->contents())->get();
    }

    /**
     * Gets the parsed data from the request's body
     *
     * @return array
     */
    public function data(): array
    {
        return $this->parsedData;
    }

    /**
     * @param string $key
     * @return ValueObject[]
     * @throws Exceptions\HandlerException
     */
    public function valueObjects(string $key): array
    {
        return $this->getHandler()->valueObjects($key);
    }

    /**
     * Get the currently set bearer token
     *
     * @return string
     */
    public function getBearerToken(): string
    {
        return $this->bearerToken;
    }

    /**
     * Set the bearer token to used with the request
     *
     * @param string $bearerToken
     */
    public function setBearerToken(string $bearerToken): void
    {
        $this->bearerToken = $bearerToken;
    }

    public function hasBearerToken(): bool
    {
        return !empty($this->bearerToken);
    }

    /**
     * @return mixed
     */
    public function getCa()
    {
        return $this->ca;
    }

    /**
     * @param mixed $ca
     */
    public function setCa($ca): void
    {
        $this->ca = $ca;
    }


    /**
     * Returns true until self::$ca is set to a valid path,
     * and this path otherwise
     * @return mixed
     */
    public function verify()
    {
        if ($this->ca === null || !file_exists($this->ca)) {
            return true;
        }

        return $this->ca;
    }

    /**
     * Returns true on non-2xx responses
     * @return bool
     */
    public function hasError(): bool
    {
        return $this->error;
    }

    /**
     * Get error message for current request
     * @return string
     */
    public function getErrorMessage(): string
    {
        // If there is no response object, return the generic error message of Grabber
        if (!isset($this->response) || !is_a($this->response, ResponseInterface::class)) {
            return $this->errorMessage;
        }

        try {
            $content = json_decode($this->contents(), true);

            if (json_last_error() !== JSON_ERROR_NONE) {
                return $this->errorMessage;
            }

            // Response has a root element error, which is string
            if (isset($content['error']) && is_string($content['error'])) {
                return $content['error'];
            }

            // Response is a laravel-responder object
            if (isset($content['error']['message'])) {
                return $content['error']['message'];
            }

        } catch (\Throwable $e) {
            return $this->errorMessage . "[{$e->getMessage()}]";
        }

        return $this->errorMessage;
    }
}
