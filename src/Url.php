<?php


namespace Bognerf\RestGrabber;

use Bognerf\RestGrabber\Exceptions\UrlException;

class Url
{

    protected $url = '';

    /**
     * Url constructor.
     *
     * @param  string $url
     * @throws UrlException
     */
    public function __construct(string $url)
    {
        $this->setUrl($url);
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param  string $url
     * @throws UrlException
     */
    protected function setUrl(string $url): void
    {
        if (filter_var($url, FILTER_VALIDATE_URL) === false) {
            throw new UrlException('`' . $url . '` is not a valid URL');
        }

        if (preg_match('/^https/', $url) !== 1) {
            throw new UrlException('Only use secure sources, my dear.');
        }

        $this->url = $url;
    }

    public function __toString()
    {
        return $this->url;
    }
}
